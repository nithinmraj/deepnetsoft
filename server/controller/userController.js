const user = require("../services/userService");

const register = (req, res) => {
    user.register(
        req.body.id,
        req.body.mailId,
        req.body.name,
        req.body.password,
        req.body.mobileNumber
    )
        .then((result) => {
            res.status(result.statusCode).json(result)
        })
}

const login = (req, res) => {
    user.login(
        req.body.mailId,
        req.body.password
    )
        .then((result) => {
            res.status(result.statusCode).json(result)
        })
}

const update = (req, res) => {
    user.update(
        req.params.id,
        req.body.mailId,
        req.body.name,
        req.body.password,
        req.body.mobileNumber
    ).then((result) => {
        res.status(result.statusCode).json(result)
    })
}

const deleteId = (req, res) => {
    user.deleteId(
        req.params.id
    ).then((result) => {
        res.status(result.statusCode).json(result)
    })
}

const list = (req, res) => {
    user.list(
        req.query
    ).then((result) => {
        res.status(result.statusCode).json(result)
    })
}

module.exports = {
    register,
    login,
    update,
    deleteId,
    list
}