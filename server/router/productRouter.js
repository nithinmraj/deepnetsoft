const route = require('express').Router()

const file = require('../middlewares/multer');

const product = require('../controller/productController')

route.post('/create', file.upload.fields([{name: 'image', maxCount: 1},{name: 'gallery',maxCount:4}]), product.create)

route.get('/read/:id', product.read)

route.patch('/update/:id',file.upload.fields([{name: 'image', maxCount: 1},{name: 'gallery',maxCount:4}]), product.update)

route.delete('/delete/:id', product.deleteId)

route.get('/list', product.list)

route.get('/search', product.search)

module.exports = route