const route = require('express').Router()

const user = require('../controller/userController')

route.post('/register',  user.register)

route.post('/login',  user.login)

route.patch('/update/:id',  user.update)

route.delete('/delete/:id', user.deleteId)

route.get('/list', user.list)

module.exports = route;