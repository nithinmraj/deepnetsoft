const db = require('../model/model');
const hash = require('../middlewares/hashGenerator')
const bcrypt = require("bcrypt");

const register = async (id, mailId, name, password, mobileNumber) => {
    const user = await db.users.findOne({
        $or: [{ "id": id }, { "mailId": mailId }]
    })
    if (user) {
        return {
            statusCode: 422,
            status: false,
            message: "exists"
        }
    }
    else {
        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(password, salt)
        const newUser = new db.users({
            id,
            mailId,
            name,
            password,
            mobileNumber,
            status: ["ACTIVE"]
        })
        newUser.save()
        console.log(`user created`);
        return {
            statusCode: 200,
            status: true,
            message: "successfully"
        }
    }
}

const login = async (mailId, password) => {
    const user = await db.users.findOne({
        mailId: mailId
    })
    if (user) {
        const validPassword = await bcrypt.compare(password, user.password);
        if (validPassword) {
            return {
                statusCode: 200,
                status: true,
                message: "successfully"
            }
        }
        return {
            statusCode: 422,
            status: false,
            message: "invalid"
        }
    }
    return {
        statusCode: 422,
        status: false,
        message: "invalid"
    }
}

const update = async (id, mailId, name, password, mobileNumber) => {
    const user = await db.users.findOne({ id })
    if (!user) {
        return {
            statusCode: 422,
            status: false,
            message: "invalid"
        }
    }
    const salt = await bcrypt.genSalt(10);
    password = await bcrypt.hash(password, salt)
    return db.users.updateOne({ id }, {
        $set: {
            mailId: mailId,
            name: name,
            password: password,
            mobileNumber: mobileNumber
        }
    }).then(() => {
        return {
            statusCode: 200,
            status: true,
            message: "successfully"
        }
    })
}

const deleteId = (id) => {
    return db.users.findOne({ id }).then((user) => {
        if (!user) {
            return {
                statusCode: 422,
                status: false,
                message: "invalid"
            }
        }
        return db.users.deleteOne({ id }).then(() => {
            return {
                statusCode: 200,
                status: true,
                message: "successfully"
            }
        })
    })
}

const list = (query) => {
    var search, sort, skip, limit;
    console.log(query);
    if (query.search) {
        search = { name: query.search };
        sort = query.sort;
        skip = parseInt(query.page)
        limit = parseInt(query.limit);
        return db.users.find(search).sort(sort).limit(limit).skip(skip).then((user) => {
            if (user) {
                return {
                    statusCode: 200,
                    status: true,
                    message: "successfully",
                    user
                }
            }
        })
    }
    if (query) {
        sort = query.sort;
        skip = parseInt(query.page)
        limit = parseInt(query.limit);
    }
    return db.users.find().sort(sort).skip(skip).limit(limit).then((user) => {
        if (!user) {
            return {
                statusCode: 422,
                status: false,
                message: "invalid"
            }
        }
        return {
            statusCode: 200,
            status: true,
            message: "successfully",
            user
        }
    })
}

module.exports = {
    register,
    login,
    update,
    deleteId,
    list
}