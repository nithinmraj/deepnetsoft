const { text } = require('express');
const mongoose = require('mongoose');

const users = mongoose.model('users', {
    id: String,
    mailId: String,
    name: String,
    password: String,
    mobileNumber: Number,
    status: Array
})

const customer = mongoose.model('customers', {
    mail: String,
    name: String,
    password: String,
    phone: String,
    date: {type: Date, default: Date.now()},
    status: {type: Boolean, default: true},
    address: { type: Array, default: [] },
    dob: Date,
    country: String,
    language: String,
    image:String
})

const products = mongoose.model('products', {
    productName: String,
    productCategory: { type: mongoose.Schema.Types.ObjectId, ref: 'categorys' },
    productSubcategory: {type: mongoose.Schema.Types.ObjectId, ref: 'subcategorys'},
    productStock: Number,
    productPrice: Number,
    productDiscount: Number,
    productVat:{type: Number, default: 0},
    productDescription: String,
    productImage: String,
    productGallery: Array,
    variations: String,
    productHighlights:String,
    ratinglength: {type: Number, default: 0},
    rating: {type: Number, default: 0},
    status: { type: Boolean, default: true },
    addToHome: {type: Boolean, default: false},
    date: { type: Date, default: Date.now() }
})

const tokens = mongoose.model('tokens', {
    userid: String,
    token: String,
    createdAt: {type: Date,expires:18000}
})

module.exports = {
    users,
    products,
    tokens,
    customer,
}